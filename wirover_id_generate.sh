#!/bin/bash
#
# This script does some one-time jobs to complete the gateway installation.
#
if [ ! -f /etc/wirover.d/node.key ]; then
    mkdir -p /etc/wirover.d

    cd /etc/wirover.d
    openssl genrsa -out node.key 2048
    openssl rsa -in node.key -pubout -out node.pub
    sha1sum node.pub | perl -n -e 'chomp; print $1 if /^([0-9a-f]+)/' >node_id

    chmod 400 node.key
    chmod 444 node.pub
    chmod 444 node_id

    chown -R wirover "/var/lib/wirover/"
fi

if [ ! -f /home/wirover/.ssh/id_rsa ]; then
    mkdir -p /home/wirover/.ssh/
    yes "" | ssh-keygen -f /home/wirover/.ssh/id_rsa -t rsa -N ''
    chown -R wirover /home/wirover/.ssh
fi
