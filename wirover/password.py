from Crypto.Hash import SHA256
import random, os

def hashPassword(password, salt = None):
    if salt == None:
        ALPHABET = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        salt = ''.join(random.choice(ALPHABET) for i in range(16))
    Hash = SHA256.new()
    Hash.update(password + salt)
    return {"hash": Hash.hexdigest(), "salt": salt}

def comparePassword(password, pwdObj):
    if(not "salt" in pwdObj or not "hash" in pwdObj):
        raise KeyError("Password file is invalid, must contain 'hash' and 'salt' keys")
    return pwdObj["hash"] == hashPassword(password, pwdObj["salt"])["hash"]
