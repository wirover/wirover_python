import re, json, argparse

POLICY_KEYS = [
    "action", "direction", "protocol",
    "local", "local_port", "local_netmask",
    "remote", "remote_port", "remote_netmask",
    "link_select", "preferred_link", "rate_limit"]
POLICY_ACTIONS = ["encap", "nat", "drop"]
POLICY_DIRECTIONS = ["ingress", "egress", "both"]
POLICY_PROTOCOLS = ["tcp", "udp", "icmp", "any"]
POLICY_LINK_SELECTS = ["weighted", "forced", "multipath", "duplicate"]

class PolicyValidationError(Exception):
    pass

class MalformedIP(PolicyValidationError):
    pass

def validateIP(ip):
    if not re.match(r"^(?:[0-9]{1,3}\.){3}[0-9]{1,3}$", ip):
        raise MalformedIP("Malformed IP address")
    return ip

def validatePolicyDict(policy_dict):
    for key in policy_dict:
        if key not in POLICY_KEYS:
            raise PolicyValidationError("Policy contains an unknown key: " + key)
    if not "action" in policy_dict or policy_dict["action"] not in POLICY_ACTIONS:
        raise PolicyValidationError("Policy must contain an action: " + POLICY_ACTIONS)

    if "direction" in policy_dict:
        if policy_dict["direction"] not in POLICY_DIRECTIONS:
            raise PolicyValidationError("Policy direction must be: " + POLICY_DIRECTIONS)

    if "protocol" in policy_dict:
        if policy_dict["protocol"] not in POLICY_PROTOCOLS:
            raise PolicyValidationError("Policy protocol must be: " + POLICY_PROTOCOLS)

    if "local" in policy_dict:
        try:
            validateIP(policy_dict["local"])
        except MalformedIP:
            raise MalformedIP("Local IP address is malformed")

    if "local_port" in policy_dict:
        if not type(policy_dict["local_port"]) is int:
            raise PolicyValidationError("Local port must be an integer")

    if "local_netmask" in policy_dict:
        try:
            validateIP(policy_dict["local_netmask"])
        except MalformedIP:
            raise MalformedIP("Local netmask is malformed")

    if "remote" in policy_dict:
        try:
            validateIP(policy_dict["remote"])
        except MalformedIP:
            raise MalformedIP("Remote IP address is malformed")

    if "remote_port" in policy_dict:
        if not type(policy_dict["remote_port"]) is int:
            raise PolicyValidationError("Remote port must be an integer")

    if "remote_netmask" in policy_dict:
        try:
            validateIP(policy_dict["remote_netmask"])
        except MalformedIP:
            raise MalformedIP("Remote netmask is malformed")

    if "link_select" in policy_dict:
        if not policy_dict["link_select"] in POLICY_LINK_SELECTS:
            raise PolicyValidationError("Policy operation must be: " + POLICY_LINK_SELECTS)

    if "rate_limit" in policy_dict:
        if not (type(policy_dict["rate_limit"]) is int or type(policy_dict["rate_limit"]) is float) or policy_dict["rate_limit"] < 0:
            raise PolicyValidationError("Policy rate limit must be a number greater than 0")

def ParsePolicy(arguments):
    policy_parser = argparse.ArgumentParser(prog = "wirover policy")
    policy_parser.add_argument("index", type=int, help = "Index to insert")
    policy_parser.add_argument("action", choices = POLICY_ACTIONS, help = "Action the policy will take")
    policy_parser.add_argument("-d", "--direction", choices = POLICY_DIRECTIONS)
    policy_parser.add_argument("-p", "--protocol", choices = POLICY_PROTOCOLS)
    policy_parser.add_argument("-ls", "--link-select", choices = POLICY_LINK_SELECTS)
    policy_parser.add_argument("-pl", "--preferred-link")
    policy_parser.add_argument("-l", "--local")
    policy_parser.add_argument("-lp", "--local-port", type=int)
    policy_parser.add_argument("-ln", "--local-netmask")
    policy_parser.add_argument("-r", "--remote")
    policy_parser.add_argument("-rp", "--remote-port", type=int)
    policy_parser.add_argument("-rn", "--remote-netmask")
    policy_parser.add_argument("-rl", "--rate-limit", type=float)
    parsed = policy_parser.parse_args(arguments)
    policy_dict = {key: value for key, value in vars(parsed).iteritems() if value != None and key != "index"}
    return parsed.index, Policy(policy_dict)

class Policy:
    def __init__(self, policy_dict):
        validatePolicyDict(policy_dict)
        for key, value in policy_dict.iteritems():
            setattr(self, key, value)
    def toDict(self):
        return {key: getattr(self, key) for key in POLICY_KEYS if hasattr(self, key)}
    def __str__(self):
        output = ""
        for key, value in self.toDict().iteritems():
            output += str(key) + ": " + str(value) + ", "
        return output[:-2]

class PolicyHelper:
    def __init__(self, daemon):
        self.daemon = daemon

    def getPolicies(self):
        return [Policy(policy_dict) for policy_dict in self.daemon.getValue("policies")]

    def insert(self, newPolicy, index):
        policy_list = self.getPolicies()
        policy_list.insert(index, newPolicy)
        self.daemon.setValue("policies", [policy.toDict() for policy in policy_list])

    def delete(self, index):
        policy_list = self.getPolicies()
        del policy_list[index]
        self.daemon.setValue("policies", [policy.toDict() for policy in policy_list])

    def __str__(self):
        output = ""
        for policy in self.getPolicies():
            output += str(policy) + "\n"
        return output[:-1]