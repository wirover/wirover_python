import os
import glob
IFE_PATH = "/var/lib/wirover/ife_list"
IFE_FORMAT = \
"""{indent}{Name}: {State}
{indent}    BW(Up/Down): {BW_Up}/{BW_Down}"""
def GetInterfaces():
    with open(IFE_PATH) as f:
        keys = f.readline().split()
        return {ife.split()[keys.index("Name")]: {keys[i]: ife.split()[i] for i in range(len(keys))} for ife in f.readlines()}

def GetInterfaceNames():
    return GetInterfaces().keys()

def StrInterface(ife, indent = 0):
    return IFE_FORMAT.format(indent = " " * indent, **ife)
