#!/usr/bin/python
import binascii
import re, json
import fcntl, subprocess
import math, time, sys, sqlite3
from collections import defaultdict
import wirover
from daemon import DaemonException
import multiprocessing
from fakedict import FakeDict
from postcache import POSTCache

IGNORED_MACS = ["3333000000fb", "01005e0000fb", "ffffffffffff"]
URLFORMAT = "http://{0}/statistics/traffic_measurements?hash={1}"
TRAFFICDB = "/var/lib/wirover/client_traffic.db"

def interface_info():
    proc = subprocess.Popen(["ifconfig"],stdout=subprocess.PIPE)
    ifconfig = ""
    for line in iter(proc.stdout.readline,''):
        ifconfig += line
    interfaces = {ife["interface"]: ife for ife in [parse_interface(interface) for interface in ifconfig.split('\n\n') if interface.strip() ]}
    return interfaces

def parse_interface(interface):
    mo = re.search(r'^(?P<interface>\S+)\s+' +
                     r'Link encap:(?P<link_encap>\S+)\s+' +
                     r'(HWaddr\s+(?P<hardware_address>\S+))?' +
                     r'(\s+inet addr:(?P<ip_address>\S+))?' +
                     r'(\s+Bcast:(?P<broadcast_address>\S+)\s+)?' +
                     r'(Mask:(?P<net_mask>\S+)\s+)?',
                     interface, re.MULTILINE )
    if mo:
        return mo.groupdict('')
    return {}

def new_client_row(client_mac, start_time):
    return {"client_mac": client_mac, "time": start_time, "rx_bytes": 0, "tx_bytes": 0,
            "packets_sent": 0, "packets_received": 0}
def update_client_row(client_row, rx_bytes, tx_bytes):
    client_row["rx_bytes"] += rx_bytes
    client_row["tx_bytes"] += tx_bytes
    if tx_bytes > 0:
        client_row["packets_sent"] += 1
    else:
        client_row["packets_received"] += 1


class TrafficClients(FakeDict):
    def __init__(self):
        FakeDict.__init__(self, self.getter, None, self.key_getter, None)
        self.conn = sqlite3.connect(TRAFFICDB)
        self.c = self.conn.cursor()

    def _clientToRow(self, client):
        return {key: value for key, value in zip(["tx_bytes", "rx_bytes", "active"], client)}

    def dict(self):
        try:
            self.c.execute("SELECT client_mac, sum(tx_bytes), sum(rx_bytes), (0 < (max(time) - strftime('%s','now', '-1 minute'))) from traffic where time > strftime('%s','now', '-1 day') group by client_mac")
            clients = self.c.fetchall()
            return {row[0]: self._clientToRow(row[1:]) for row in clients}
        except sqlite3.OperationalError:
            return None

    def getter(self, name):
        try:
            self.c.execute("SELECT sum(tx_bytes), sum(rx_bytes), (0 < (max(time) - strftime('%s','now', '-1 minute'))) from traffic where time > strftime('%s','now', '-1 day') and client_mac = ?", [name])
            client = self.c.fetchone()
            if client == None: return None
            return self._clientToRow(client)
        except sqlite3.OperationalError:
            return None

    def key_getter(self):
        try:
            self.c.execute("SELECT distinct client_mac from traffic where time > strftime('%s','now', '-1 day')")
            return [row[0] for row in self.c.fetchall()]
        except sqlite3.OperationalError:
            return []

    def stats_24_hours(self):
        try:
            self.c.execute("""select sum(tx_bytes) as tx_bytes, sum(rx_bytes) as rx_bytes,
                                  count(DISTINCT client_mac) as users, (cast(time as integer) / 3600) * 3600 AS roundtime
                              from traffic where time > strftime('%s','now', '-1 day') group by roundtime""")
            return [{col[0]: row[i] for i, col in enumerate(self.c.description)} for row in self.c.fetchall()]
        except sqlite3.OperationalError:
            return []
    

class Traffic(multiprocessing.Process):
    def __init__(self, WiRover, device):
        multiprocessing.Process.__init__(self)
        self.wirover = WiRover
        self.log = WiRover.getLogger("traffic_logger")
        self.device = device
        self.conn = sqlite3.connect(TRAFFICDB)
        subprocess.call("chown wirover {0}".format(TRAFFICDB), shell = True)
        self.c = self.conn.cursor()
        self.c.execute("CREATE TABLE IF NOT EXISTS traffic (client_mac text, time real, interval real, tx_bytes int, rx_bytes int)")

        interfaces = interface_info()
        if device not in interfaces:
            raise Exception("Interface not found {0}", device)
        self.ife_mac = interfaces[device]["hardware_address"].replace(":","")
        self.post_url = None
        try:
            self.post_url = URLFORMAT.format(self.wirover.settings["wiroot_address"], self.wirover.node_hash)
        except DaemonException as e:
            self.log.error(e)
            exit(1)

        self.postcache = POSTCache(
            self.post_url,
            "/var/lib/wirover/traffic_cache.db",
            async_interval = 30)
        
    def run(self):
        import pcap
        import dpkt
        ife_mac = self.ife_mac
        pc = pcap.pcap(self.device)
        pc.setfilter("tcp or udp")
        tmp_packets_db = {}
        tmp_packets_post = {}
        last_interval = time.time()
        last_post = time.time()
        for ts, pkt in pc:
            pkt = dpkt.ethernet.Ethernet(pkt)
            src = binascii.hexlify(pkt.src)
            dst = binascii.hexlify(pkt.dst)
            tx_bytes = 0
            rx_bytes = 0
            client = None
            if src == ife_mac:
                client = dst
                rx_bytes = len(pkt.data)
            else:
                client = src
                tx_bytes = len(pkt.data)
            if client in IGNORED_MACS:
                continue
            if not client in tmp_packets_db:
                tmp_packets_db[client] = new_client_row(client, last_interval)
            update_client_row(tmp_packets_db[client], rx_bytes, tx_bytes)
            if ts - last_interval > 2:
                for row in tmp_packets_db.values():
                    row["duration"] = ts - last_interval
                    self.c.execute("INSERT INTO traffic VALUES (\"{client_mac}\", {time}, {duration}, {tx_bytes}, {rx_bytes})".format(**row))
                    self.conn.commit()
                tmp_packets_db = {}
                last_interval = ts
            if self.post_url != None:
                if not client in tmp_packets_post:
                    tmp_packets_post[client] = new_client_row(client, last_post)
                update_client_row(tmp_packets_post[client], rx_bytes, tx_bytes)
                if ts - last_post > 30:
                    client_rows = tmp_packets_post.values()
                    for row in client_rows:
                        row["duration"] = ts - last_post
                        self.postcache.add_request(row)
                        
                    tmp_packets_post = defaultdict(list)
                    last_post = ts
