import dbus, os

class ServiceException(Exception):
    pass

class Service(object):
    def __init__(self, job_name):
        if job_name[0] == '/':
            raise ValueError("Expected simple, short job name: %s" % 
                             (job_name))

        self.__system = dbus.SystemBus()
        self.job_name = job_name
        self.override_path = "/etc/init/" + self.job_name + ".override"
        upstart = dbus.Interface(self.__system.get_object(
                                    'com.ubuntu.Upstart', 
                                    '/com/ubuntu/Upstart'),
                                  'com.ubuntu.Upstart0_6')
        try:
            upstart.GetJobByName(job_name)
        except dbus.exceptions.DBusException:
            raise ServiceException("Job not found {0}".format(job_name))
        self.__o = self.__system.get_object(
                'com.ubuntu.Upstart', 
                '/com/ubuntu/Upstart/jobs/%s' % (self.job_name))

        self.__job_i = dbus.Interface(
                        self.__o, 
                        'com.ubuntu.Upstart0_6.Job')

    def running(self):
        return len(self.__job_i.GetAllInstances()) > 0

    def is_manual(self):
        return os.path.isfile(self.override_path)

    def auto(self):
        if not self.is_manual(): return
        os.remove(self.override_path)

    def manual(self):
        if self.is_manual(): return
        with open(self.override_path, 'w') as f:
            f.write("manual\n")

    def start(self):
        self.__job_i.Start('', True)

    def stop(self):
        self.__job_i.Stop('', True)

    def restart(self):
        self.__job_i.Restart('', True)
