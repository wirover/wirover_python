import jrpc
from setting import *
import json 
import urllib2
from cloud import CloudConfig
from service import Service
from pubkey import PubkeyHelper

CONFIG_PATH = "/var/lib/wirover/config.json"

class DaemonException(Exception):
    pass

class ConfigDaemon(jrpc.service.SocketObject):
    def __init__(self, wirover):
        import dblog
        jrpc.service.SocketObject.__init__(self, 8079)
        self.wirover = wirover
        self.settings = {"debug": EditConfigFile("Debug", "debug\\s*=\\s*", self.wirover.config, valueType = int),
                         "log_level": LogLevelSetting(CONFIG_PATH),
                         "wiroot_address": WiRootAddress(self.wirover.config),
                         "wiroot_port": EditConfigFile("WiRoot Port", "wiroot-port\\s*=\\s*", self.wirover.config),
                         "mtu": EditConfigFile("Tunnel MTU", "mtu\\s*=\\s*", self.wirover.config, valueType = int),
                         "reboot": LocalReboot(),
                         "remote_reboot": RemoteReboot(CONFIG_PATH)}

        log_db_uri = None
        try:
            wiroot_address = self.settings["wiroot_address"].getValue()
            if wiroot_address != "":
                log_db_uri = "http://" + wiroot_address + "/log"
        except:
            pass

        self.log_service = dblog.LoggingService("/var/lib/wirover/log_cache.db", log_db_uri, log_level = self.settings["log_level"].getValue())

        self.logger = dblog.Logger(self.log_service.log, tags = {'node_hash': wirover.node_hash, 'module': 'wiroverdaemon'})
        self.services = {}
        self._addService("wiroverdaemon")
        self.cloud = None
        debug = False
        try:
            debug = self.settings["debug"].getValue()
        except SettingException:
            debug = False

        if self.wirover.is_gateway:
            self._addService("wigateway")
            self._addService("hostapd")
            self._addService("dnsmasq")
            self.settings["ssid"] = EditConfigFile("Wireless SSID", "ssid=", "/etc/hostapd/hostapd.conf")
            self.settings["wireless_password"] = WirelessPasswordSetting()
            self.settings["policies"] = LocalPolicySetting(CONFIG_PATH)
            self.settings["remote_policies"] = RemotePolicySetting(CONFIG_PATH)
            self.settings["virt_wifi_networks"] = WiFiVirtClient()
            self.settings["dashboard_password"] = JSONFileSetting("/var/lib/wirover/dashboard.pwd", "Dashboard Password")
        if self.wirover.is_rootserver:
            self._addService("wiroot")
        if self.wirover.is_controller:
            self._addService("wicontroller")

        self.pubkeyHelper = PubkeyHelper("wirover")

    def pre_run(self):
        jrpc.service.SocketObject.pre_run(self)
        self.logger.info("Daemon Started")
        if self.wirover.is_gateway:
            try:
                wiroot = self.settings["wiroot_address"].getValue()
                self.cloud = CloudConfig(wiroot, self.wirover.node_hash, self.settings, self.logger)
            except SettingException:
                self.logger.error("Root server not found, cloud service is disabled")

    def close(self):
        if hasattr(self, 'cloud') and self.cloud != None:
            self.cloud.close()
        jrpc.service.SocketObject.close(self)
        self.logger.info("Daemon Stopping")


    def _addService(self, service):
        try:
            self.services[service] = Service(service)
        except Exception as e:
            self.logger.error(e)

    @jrpc.service.method
    def setValue(self, name, value):
        if name not in self.settings or not self.settings[name].local:
            return False
        self.settings[name].setValue(value)
        if self.settings[name].remote and self.cloud:
            self.cloud.onSettingChanged(name, value)
        return True

    @jrpc.service.method
    def getValue(self, name):
        if not name in self.settings or not self.settings[name].local:
            raise KeyError("Could not find setting {0}".format(name))
        return self.settings[name].getValue()

    @jrpc.service.method
    def getSettings(self):
        return [key for key, setting in self.settings.iteritems() if setting.local and not key == "policies"]

    @jrpc.service.method
    def getSettingsInfo(self):
        return {name: {"name": str(prop.name), "type": str(prop.value_type)} for name, prop in self.settings.iteritems() if prop.local}

    @jrpc.service.method
    def getServices(self):
        return self.services.keys()

    @jrpc.service.method
    def getServiceRunning(self, service):
        if not service in self.services:
            self._throwAndLog("Could not find service {0}".format(service))
        service_obj = self.services[service]
        return {"running": service_obj.running(), "manual": service_obj.is_manual()}

    @jrpc.service.method
    def setServiceRunning(self, service, value):
        if not service in self.services:
            raise KeyError("Could not find service {0}".format(service))
        if not value in ["start", "stop", "restart", "manual", "auto"]:
            raise RuntimeError("Valid options are start, stop, restart, manual or auto not " + value)
        if value == "start":
            self.services[service].start()
        elif value == "stop":
            self.services[service].stop()
        elif value == "restart":
            self.services[service].restart()
        elif value == "manual":
            self.services[service].manual()
        elif value == "auto":
            self.services[service].auto()

    @jrpc.service.method
    def getPubkeyNames(self):
        return self.pubkeyHelper.getPubkeys().keys()

    @jrpc.service.method
    def getPubkeyAuthorized(self, name):
        return name in self.pubkeyHelper.getContainedNames()

    @jrpc.service.method
    def setPubkeyAuthorized(self, name, value):
        if value:
            self.pubkeyHelper.authPubkey(name)
        else:
            self.pubkeyHelper.revokePubkey(name)

    @jrpc.service.method
    def setPubkey(self, name, pubkey):
        self.pubkeyHelper.setPubkey(name, pubkey)

    @jrpc.service.method
    def getPubkey(self, name):
        return self.pubkeyHelper.getPubkeys()[name]

    @jrpc.service.method
    def deletePubkey(self, name):
        self.pubkeyHelper.deletePubkey(name)

