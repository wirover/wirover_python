import os
from os.path import isfile, join
class PubkeyHelper:
    def __init__(self,username,pubkeydir="/var/lib/wirover/pubkeys/"):
        self.username = username
        self.pubkeydir = pubkeydir
        
        self.authorizedKeysPath = "/home/{0}/.ssh/authorized_keys".format(self.username)

    def getPubkeys(self):
        output = {}
        if os.path.exists(self.pubkeydir):
            pubkeyfiles = [ f for f in os.listdir(self.pubkeydir) if isfile(join(self.pubkeydir,f)) and ".pub" in f ]
            for fname in pubkeyfiles:
                pubkey = None
                try:
                    with open(join(self.pubkeydir,fname)) as f:
                        pubkey = f.read()
                except:
                    raise IOError("Unabled to open pubkey file {0}".format(f))
                output[fname.split(".")[0]] = pubkey
        return output

    def getContainedNames(self):
        contained = []
        try:
            with file(self.authorizedKeysPath,"r") as f:
                for line in f.readlines():
                    for name, pubkey in self.getPubkeys().iteritems():
                        if pubkey == line and name not in contained:
                            contained += [name]
        except IOError:
            pass
        return contained

    def authPubkey(self, name):
        pubkeydict = self.getPubkeys()
        if not name in pubkeydict:
            raise Exception("Name not recognized")
        if name in self.getContainedNames():
            raise Exception("Pubkey already authorized")
        try:
            if not os.path.exists(os.path.dirname(self.authorizedKeysPath)):
                os.makedirs(os.path.dirname(self.authorizedKeysPath))
        except:
            raise IOError("Could not create .ssh directory")
        with file(self.authorizedKeysPath,'a') as f:
            f.write(pubkeydict[name])
            return True
        raise Exception("Failed to add key")

    def revokePubkey(self, name):
        pubkeydict = self.getPubkeys()
        if not name in pubkeydict:
            raise Exception("Name not recognized")
        if not name in self.getContainedNames():
            raise Exception("Pubkey not currently authorized")
        removeKey = pubkeydict[name]
        newText = ""
        with file(self.authorizedKeysPath,"r") as f:
            for line in f.readlines():
                if line != removeKey:
                    newText += line
        with file(self.authorizedKeysPath,'w') as f:
            f.write(newText)
            return True
        raise Exception("Failed to remove key")

    def deletePubkey(self, name):
        os.remove(join(self.pubkeydir,name+".pub"))

    def setPubkey(self, name, pubkey):
        with file(join(self.pubkeydir,name+".pub"),'w') as f:
            f.write(pubkey+"\n")
            return True