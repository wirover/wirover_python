import os, shutil
import subprocess
import requests
from requests.exceptions import HTTPError
import time

UPGRADE_URL_FORMAT = "http://{0}/upgrade/{1}.tar.bz2"
UPGRADE_DIR = "/var/lib/wirover/upgrade/"

def upgrade(install_type):
    import wirover
    wirover = wirover.WiRover()
    logger = wirover.getLogger(module="wirover.upgrade")
    wiroot_address = wirover.settings["wiroot_address"]
    if wiroot_address == "":
        logger.error("An upgrade was attempted before the root server address was configured")
        return 1

    
    url = UPGRADE_URL_FORMAT.format(wiroot_address, install_type)
    if os.path.exists(UPGRADE_DIR):
        shutil.rmtree(UPGRADE_DIR)
    os.makedirs(UPGRADE_DIR)

    try:
        download_file(url)

        subprocess.call("tar -xf package.tar.bz2".split(), cwd = UPGRADE_DIR)
        subprocess.call("rm package.tar.bz2".split(), cwd = UPGRADE_DIR)
        error = subprocess.call("./install.py", shell = True, cwd = UPGRADE_DIR)
        if error != 0: return error
        subprocess.call("reboot", shell = True)
    except HTTPError as e:
        logger.error("An error occured while updating {0}".format(str(e)))
        return 1
    return 0


def download_file(url):
    local_filename = os.path.join(UPGRADE_DIR, "package.tar.bz2")
    r = requests.get(url, stream=True)
    r.raise_for_status()
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
                f.flush()
