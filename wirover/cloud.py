import json
import requests
import threading
import time
from fakedict import JSONFile

URLFORMAT = "http://{0}/configure_gateways/configForGatewayHash?hash={1}"
DIRTY_PATH = "/var/lib/wirover/dirty_settings.json"

class CloudPostTask(threading.Thread):
    def __init__(self, url, data, onSuccess, onFailure):
        threading.Thread.__init__(self)
        self.url = url
        self.data = data
        self.onSuccess = onSuccess
        self.onFailure = onFailure
        
    def run(self):
        try:
            response = requests.post(self.url,
                                  json=self.data)
            if response.status_code != 200:
                raise Exception("Server responded with error: {0}".format(response.status_code))
            if self.onSuccess:
                self.onSuccess(self.data)
            return
        except Exception as e:
            if self.onFailure:
                self.onFailure(str(e))
            return

class CloudConfig(threading.Thread):
    def __init__(self, server, node_hash, settings, log):
        threading.Thread.__init__(self)
        self.server = server
        self.node_hash = node_hash
        self.url = URLFORMAT.format(server, node_hash)
        self.settings = settings
        self.running = True
        self.log = log
        self.dirtyFile = JSONFile(DIRTY_PATH)
        self.dirtyLock = threading.Lock()
        self.start()

    def run(self):
        while self.running:
            serverSettings = self.pullServerConfig()
            self.dirtyLock.acquire()
            try:
                dirtySettings = self.dirtyFile.dict()
                if dirtySettings:
                    CloudPostTask(self.url, dirtySettings, self.onPostSuccess, self.onPostFailure).start()
                    self.log.debug("Trying to post dirty settings again before pulling")
                elif serverSettings:
                    self.log.debug("Received server settings: " + str(serverSettings))
                    for name, value in serverSettings.iteritems():
                        try:
                            if name in self.settings and self.settings[name].remote:
                                if not self.settings[name].compare(value):
                                    self.settings[name].setValue(value)
                                    self.log.info("Updating {0} to {1}".format(name, value), tags = {"action": "update"})
                            else:
                                self.log.warning("Got unknown setting {0}".format(name))
                        except Exception as e:
                            self.log.error("Setting {0} caused an error:\n{1}".format(name, str(e)))
            except:
                pass
            self.dirtyLock.release()
            for _ in range(100):
                time.sleep(.1)
                if not self.running: break

    def close(self):
        self.running = False

    def onPostSuccess(self, data):
        self.dirtyLock.acquire()
        for name, value in data.iteritems():
            if name in self.dirtyFile and self.dirtyFile[name] == value:
                del self.dirtyFile[name]
        self.log.info("Settings posted to server: " + str(data), tags = {"action": "post"})
        self.dirtyLock.release()

    def onPostFailure(self, error):
        self.log.info(error)

    def onSettingChanged(self, setting, newValue):
        self.dirtyLock.acquire()
        self.dirtyFile[setting] = newValue
        dirtySettings = self.dirtyFile.dict()
        self.dirtyLock.release()
        CloudPostTask(self.url, dirtySettings, self.onPostSuccess, self.onPostFailure).start()
        self.log.debug("Started a post task for setting change")

    def pullServerConfig(self):
        try:
            result = requests.get(self.url)
            return result.json()
        except:
            return {}
