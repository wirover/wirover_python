import re, os
from fakedict import JSONFile
import json
from wiroverwificlient import wiroverwificlient
import subprocess

TEMPLATE_PATH = "/var/lib/wirover/config_templates/"

class SettingException(Exception):
    pass

class Setting(object):
    def __init__(self, name, valueType = str, local = True, remote = True):
        self.name = name
        self.value_type = valueType
        self.local = local
        self.remote = remote
    def set(self, value):
        pass
    def get(self):
        return None
    def getValue(self):
        try:
            return self.get()
        except Exception as e:
            self.handleError(e)
            return self.get()
    def compare(self, value):
        return self.getValue() == self.value_type(value)
    def setValue(self, value, default = None):
        if value == None:
            value = default
        else:
            value = self.value_type(value)
        try:
            return self.set(value)
        except Exception as e:
            self.handleError(e)
            return self.set(value)
    def handleError(self, e):
        raise e

class EditConfigFile(Setting):
    def __init__(self, name, regex, path, valueType = str, local = True, remote = True):
        Setting.__init__(self, name, valueType, local, remote)
        self.regex = regex
        self.path = path
        if(self.path != None):
            self.templatePath = os.path.join(TEMPLATE_PATH, self.path.lstrip("/"))
        else:
            self.templatePath = None

    @staticmethod
    def GetFileValue(path, regex, name):
        regex = r"[#\s]*?({0}[\s'\"]*)(.*?)(['\"]*?[;]?[\s]*$)".format(regex)
        lines = []
        with open(path,'r') as f:
            lines = f.readlines()
        for i in range(len(lines)):
            match = re.match(regex,lines[i])
            if match != None:
                return match.group(2)
        raise SettingException("Setting \"{0}\" could not be found in {1}".format(name, path))

    @staticmethod
    def SetFileValue(value, path, regex, name):
        regex = "[#\s]*?(" + regex + "[\s']*\"{0,1})(.*?)(['\"]*?[;]?[\s]*$)"
        lines = []
        with open(path,'r') as f:
            lines = f.readlines()
        if len(lines) == 0: raise SettingException("File {0} is empty".format(path))
        matched = False
        for i in range(len(lines)):
            match = re.match(regex,lines[i])
            if match != None:
                matched = True
                lines[i] = lines[i][match.start(1):match.start(2)] + str(value) + lines[i][match.start(3):match.end(3)+1]
                break
        if not matched: raise SettingException("Setting \"{0}\" could not be found in {1}".format(name, path))
        with open(path + '.swp','w') as f:
            f.writelines(lines)
        os.rename(path + '.swp', path)

    def handleError(self, e):
        if(self.templatePath == None): raise e
        if(any([isinstance(e, exc) for exc in [IOError, SettingException]]) and os.path.exists(self.templatePath)):
            subprocess.call("rsync {0} {1}".format(self.templatePath, self.path).split())
        else:
            raise e
    def set(self, value):
        return EditConfigFile.SetFileValue(value, self.path, self.regex, self.name)
    def get(self):
        if(self.path == None):
            raise SettingException("Config file setting doesn't contain a path")
        try:
            return self.value_type(EditConfigFile.GetFileValue(self.path, self.regex, self.name))
        except TypeError:
            raise SettingException("Value in file is of an incorrect type")

class JSONFileSetting(Setting):
    def __init__(self, path, *args, **kwargs):
        if(not "valueType" in kwargs):
            kwargs["valueType"] = lambda v: v
        Setting.__init__(self, *args, **kwargs)
        self.path = path
    def set(self, value):
        with open(self.path, 'w') as f:
            json.dump(value, f)
    def get(self):
        if(not os.path.exists(self.path)):
            return None
        with open(self.path, 'r') as f:
            return json.load(f)
    def handleError(self, e):
        if isinstance(e, ValueError):
            with open(self.path, 'w') as f:
                json.dump({}, f)

#The wiroot_address setting is backed up in the template as well
class WiRootAddress(EditConfigFile):
    def __init__(self, path):
        EditConfigFile.__init__(self, "WiRoot Address", "wiroot-address\\s*=\\s*", path, remote = False)
    def set(self, value):
        if(os.path.exists(self.templatePath)):
            EditConfigFile.SetFileValue(value, self.templatePath, self.regex, self.name)
        EditConfigFile.set(self, value)

class LogLevelSetting(Setting):
    def __init__(self, config_data):
        self.jsonFile = JSONFile(config_data)
        Setting.__init__(self, "Log Level", valueType = str)
    def set(self, value):
        self.jsonFile["log_level"] = value  
    def get(self):
        if "log_level" not in self.jsonFile:
            self.jsonFile["log_level"] = "warning"
        return self.jsonFile["log_level"]

class RemoteReboot(Setting):
    def __init__(self, config_data):
        self.jsonFile = JSONFile(config_data)
        Setting.__init__(self, "Remote Reboot", valueType = int, local = False)
        
    def set(self, value):
        if "lastRebooted" not in self.jsonFile:
            self.jsonFile["lastRebooted"] = value
        if self.jsonFile["lastRebooted"] < value:
            self.jsonFile["lastRebooted"] = value
            os.popen("shutdown -a -r now")

    def get(self):
        if "lastRebooted" not in self.jsonFile:
            self.jsonFile["lastRebooted"] = 0
        return self.jsonFile["lastRebooted"]

class LocalReboot(Setting):
    def __init__(self):
        Setting.__init__(self, "Reboot", remote = False, valueType = int)

    def set(self, value):
        if value:
            os.popen("shutdown -a -r now")

class WirelessPasswordSetting(Setting):
    def __init__(self):
        Setting.__init__(self, "Wireless Password")
        self.path = "/etc/hostapd/hostapd.conf"
    def set(self, value):
        if not value:
            EditConfigFile.SetFileValue("0", self.path, "wpa=", self.name)
        else:
            EditConfigFile.SetFileValue("2", self.path, "wpa=", self.name)
            EditConfigFile.SetFileValue(value, self.path, "wpa_passphrase=", self.name)
    def get(self):
        wpa = EditConfigFile.GetFileValue(self.path, "wpa=", self.name)
        if wpa == "0":
            return None
        if wpa == "2":
            return EditConfigFile.GetFileValue(self.path, "wpa_passphrase=", self.name)
    def compare(self, value):
        if self.getValue() == None and value == '': return True
        return Setting.compare(self, value)

class WiFiVirtClient(Setting):
    def __init__(self):
        Setting.__init__(self, "Wireless Networks", valueType = list)
        self.path = "/etc/wpa_supplicant.json"
    def set(self,value):
        wiroverwificlient.setListOfWiFiNetworks(value)
    def get(self):
        return wiroverwificlient.getListOfWiFiNetworks()

class PolicySetting(Setting):
    def __init__(self, config_path, *args, **kwargs):
        kwargs["valueType"] = list
        Setting.__init__(self, *args, **kwargs)
        self.policy_path = "/var/lib/wirover/policy_tbl"
        self.config_data = JSONFile(config_path)
    def set(self, value):
        with open(self.policy_path,'w') as f:
            json.dump(value, f, indent=4)
    def get(self):
        if not os.path.exists(self.policy_path): return []
        with open(self.policy_path,'r') as f:
            return json.load(f)

class LocalPolicySetting(PolicySetting):
    def __init__(self, config_path):
        PolicySetting.__init__(self, config_path, "Policies", remote=False)
    def set(self, value):
        self.config_data["use_remote_policies"] = False
        PolicySetting.set(self, value)

class RemotePolicySetting(PolicySetting):
    def __init__(self, config_path):
        PolicySetting.__init__(self, config_path, "Remote Policies", local = False)
    def set(self, value):
        if "use_remote_policies" in self.config_data and not self.config_data["use_remote_policies"]:
            return
        PolicySetting.set(self, value)
    def compare(self, value):
        if "use_remote_policies" in self.config_data and not self.config_data["use_remote_policies"]:
            return True
        return PolicySetting.compare(self, value)
