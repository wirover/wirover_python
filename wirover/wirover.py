import os
from fakedict import FakeDict
import interface
import jrpc
import json
from policy import PolicyHelper

class WiRover(object):
    def __init__(self):
        self.is_gateway = False
        self.is_controller = False
        self.is_rootserver = False
        self.daemon = jrpc.service.SocketProxy(8079)
        self.settings = FakeDict(self.daemon.getValue,
                                 self.daemon.setValue,
                                 self.daemon.getSettings)
        debug = False
        try:
            debug = self.settings["debug"]
        except jrpc.exception.JRPCError:
            debug = False

        self.services = FakeDict(self.daemon.getServiceRunning,
                                 self.daemon.setServiceRunning,
                                 self.daemon.getServices)

        self.authorized = FakeDict(self.daemon.getPubkeyAuthorized,
                                   self.daemon.setPubkeyAuthorized,
                                   self.daemon.getPubkeyNames)

        self.pubkeys = FakeDict(self.daemon.getPubkey,
                                self.daemon.setPubkey,
                                self.daemon.getPubkeyNames,
                                self.daemon.deletePubkey)

        self.policies = PolicyHelper(self.daemon)
        
        self.node_hash = None
        self.config = None
        if os.path.exists("/etc/wigateway.conf"):
            from traffic import TrafficClients
            self.interfaces = FakeDict(lambda name: interface.GetInterfaces()[name],
                                   None,
                                   interface.GetInterfaceNames)
            self.traffic = TrafficClients()
            self.config = "/etc/wigateway.conf"
            self.is_gateway = True

        if os.path.exists("/etc/wiroot.conf"):
            self.config = "/etc/wiroot.conf"
            self.is_rootserver = True
            
        if os.path.exists("/etc/wicontroller.conf"):
            self.config = "/etc/wicontroller.conf"
            self.is_controller = True

        try:
            with open("/etc/wirover.d/node_id",'r') as f:
                self.node_hash = f.readline()
        except:
            self.is_valid = False

    def getLogger(self, module, tags = None):
        from dblog import Logger
        if tags == None: tags = {}
        tags["module"] = module
        tags["node_hash"] = self.node_hash
        return Logger(self.daemon.log_service.log, tags)

    def __str__(self):
        wirover_type = ""
        if self.is_gateway:
            wirover_type = "Gateway"
        elif self.is_controller:
            wirover_type = "Controller"
        elif self.is_rootserver:
            wirover_type = "Root Server"
        output = wirover_type
        try:
            settings_dict = self.settings.dict()
            authorized_dict = self.authorized.dict()
            output += "\nSettings:"
            for p in self.settings:
                output += "\n    " + p + ": " + str(self.settings[p])
            output += "\nServices:"
            for service, running in self.services.dict().iteritems():
                output += "\n    " + service + ": " + ("Running" if running else "Stopped")
            output += "\nAuthorized Pubkeys:"
            for name in [auth for auth in authorized_dict if authorized_dict[auth]]:
                output += "\n    " + name
        except jrpc.exception.JRPCError:
            output += "\nDaemon not running"
        if self.is_gateway:
            output += "\nInterfaces:"
            try:
                for ife in self.interfaces.dict().values():
                    output += "\n" + interface.StrInterface(ife, indent = 4)
            except IOError:
                output += "\n    IO Error"
            output += "\nClient count: " + str(len(self.traffic.keys()))
        return output
