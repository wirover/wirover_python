#!/usr/bin/python2.7
from wirover import *
from wirover import traffic as traffic
from wirover.daemon import DaemonException
import argparse
import json, jrpc
from wirover import policy as Policy
import sys

def convert(type_):
    type_ = type_.split("'")[1]
    import importlib
    # Check if it's a builtin type
    module = importlib.import_module('__builtin__')
    return getattr(module, type_)

parser = argparse.ArgumentParser(prog = "wirover")
parser.add_argument("action", choices = ["get", "set", "service", "policy", "status", "daemon", "traffic_logger", "pubkey", "upgrade"], help = "Command action")
args = parser.parse_args(sys.argv[1:2])
remaining_args = sys.argv[2:]
if args.action == "status":
    wirover = wirover.WiRover()
    print wirover
elif args.action == "daemon":
    from wirover.daemon import ConfigDaemon
    wirover = wirover.WiRover()
    daemon = ConfigDaemon(wirover)
    daemon.run_wait()
elif args.action == "traffic_logger":
    traffic_parser = argparse.ArgumentParser(prog = "wirover traffic_logger")
    traffic_parser.add_argument("name", nargs = "?", default = None, help = "Interface name")
    args = traffic_parser.parse_args(remaining_args)
    if args.name == None:
        print "Traffic logger requires name of interface"
        exit(1)
    wirover = wirover.WiRover()
    traffic_logger = traffic.Traffic(wirover, args.name)
    traffic_logger.run()
elif args.action == "get":
    setting_parser = argparse.ArgumentParser(prog = "wirover get")
    setting_parser.add_argument("name", help = "Setting name")
    args = setting_parser.parse_args(remaining_args)
    wirover = wirover.WiRover()
    print wirover.settings[args.name]
    exit(0)
elif args.action == "set":
    setting_parser = argparse.ArgumentParser(prog = "wirover set")
    setting_parser.add_argument("name", help = "Setting name")
    setting_parser.add_argument("value", help = "Value the setting should take")
    args = setting_parser.parse_args(remaining_args)
    if args.name == None or args.value == None:
        print "Setting and value are required for action set"
        exit(1)
    wirover = wirover.WiRover()
    daemon = wirover.daemon
    try:
        info = daemon.getSettingsInfo()
    except jrpc.exception.JRPCError:
        print "Daemon not running"
        exit(1)
    if args.name not in info:
        print "Invalid setting {0}".format(args.name)
        exit(1)
    setting_info = info[args.name]
    t = convert(setting_info["type"])
    value = None
    try:
        value = t(args.value)
    except:
        print "{name} must be {type}".format(**setting_info)
        exit(1)
    wirover.settings[args.name] = value
    
    print "Success"
elif args.action == "service":
    service_parser = argparse.ArgumentParser(prog = "wirover service")
    service_parser.add_argument("name", help = "Service name")
    service_parser.add_argument("action", choices = ["start", "stop", "restart"], help = "Service operation to perform")
    args = service_parser.parse_args(remaining_args)
    wirover = wirover.WiRover()
    if args.name not in wirover.services:
        print "Unknown service"
        exit(1)
    try:
        wirover.services[args.name] = args.action
    except DaemonException as e:
        print e
        exit(1)
    print "Success"
elif args.action == "pubkey":
    pubkey_parser = argparse.ArgumentParser(prog = "wirover pubkey")
    pubkey_parser.add_argument("pubkey_action", choices = ["list", "authorize","revoke","add", "delete"], help = "Pubkey operation to perform")
    pubkey_parser.add_argument("name", nargs="?", default = None, help = "User's name")
    pubkey_parser.add_argument("pubkey", nargs="?", default = None, help = "User's pubkey when adding")
    args = pubkey_parser.parse_args(remaining_args)
    wirover = wirover.WiRover()
    if args.pubkey_action == "list":
        print wirover.pubkeys.dict().keys()
        exit(0)
    try:
        if args.pubkey_action == "authorize":
            wirover.authorized[args.name] = 1
        elif args.pubkey_action == "revoke":
            wirover.authorized[args.name] = 0
        elif args.pubkey_action == "add":
            if args.pubkey == None:
                print "Error: Must specify pubkey when adding"
                pubkey_parser.print_help()
                exit(1)
            wirover.pubkeys[args.name] = args.pubkey
        elif args.pubkey_action == "delete":
            del wirover.pubkeys[args.name]

    except Exception as e:
        print "Error:", e
        exit(1)
    print "Success"
elif args.action == "policy":
    policy_parser = argparse.ArgumentParser(prog = "wirover policy")
    policy_parser.add_argument("action", choices = ["list", "insert", "delete"], help = "Policy operation to perform")
    if(len(remaining_args) == 0):
        policy_parser.parse_args(remaining_args)
        exit(1)
    args = policy_parser.parse_args(remaining_args[0:1])
    remaining_args = remaining_args[1:]
    wirover = wirover.WiRover()
    if args.action == "list":
        print wirover.policies
    elif args.action == "insert":
        policy_insert_parser = argparse.ArgumentParser(prog = "wirover policy insert")
        if(len(remaining_args) == 0):
            policy_parser.parse_args(remaining_args)
        index, policy = Policy.ParsePolicy(remaining_args)
        wirover.policies.insert(policy, index)
        print wirover.policies
    elif args.action == "delete":
        policy_insert_parser = argparse.ArgumentParser(prog = "wirover policy delete")
        policy_insert_parser.add_argument("index", type=int, help = "Index to delete")
        args = policy_insert_parser.parse_args(remaining_args)
        wirover.policies.delete(args.index)
        print wirover.policies
elif args.action == "upgrade":
    from wirover.upgrade import upgrade
    exit(upgrade("wigateway"))
